0.3.1
- fix: update notification title on task title update if running
0.3.0
- reset action added to context menu
- reset action now requires confirmation