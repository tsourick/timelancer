package com.helloworldstudio.timelancer;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.Comparator;

/**
 * Created by pe60 on 30.06.2017.
 */

public class TaskListViewAdapter extends ArrayAdapter<Task> {
    public enum STATE {
        VIRGIN, // initial, onReset
        DEFAULT, // at least started once, no matter running or not
        RUNNING  // running
    }


    private final Context  context;
    private final Tasks tasks;

    public void log (String s){
        // Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        Log.d("debuglog", "TaskListViewAdapter: " + s);
    }

    public TaskListViewAdapter(Context context, Tasks tasks) {
        super(context, -1, tasks.asList());

        this.context = context;
        this.tasks = tasks;
    }

    public final static int SORT_ASC  = 1;
    public final static int SORT_DESC = -1;

    private static int getColor(int resId) {
        try {
            return ContextCompat.getColor(TLApplication.getAppContext(), resId);
        }
        catch(Exception e){
            return Color.parseColor("#000000");
        }

    }

    public void sortByTitle(int sortDirection) {
        Comparator<Task> cpr;

        if (sortDirection == SORT_DESC){
            cpr = new Comparator<Task>() {
                @Override
                public int compare(Task object1, Task object2) {
                    return object2.title.compareTo(object1.title);
                }
            };
        } else {
            cpr = new Comparator<Task>() {
                @Override
                public int compare(Task object1, Task object2) {
                    return object1.title.compareTo(object2.title);
                }
            };
        }

        this.sort(cpr);
    }

    public void sortByTimestamp(int sortDirection) {
        Comparator<Task> cpr;

        if (sortDirection == SORT_DESC){
            cpr = new Comparator<Task>() {
                @Override
                public int compare(Task object1, Task object2) {
                    log(String.format("compare %s to %s", object2.ts, object1.ts));
                    return object1.ts.compareTo(object2.ts);
                }
            };
        } else {
            cpr = new Comparator<Task>() {
                @Override
                public int compare(Task object1, Task object2) {
                    log(String.format("compare %s to %s", object1.ts, object2.ts));
                    return object2.ts.compareTo(object1.ts);
                }
            };
        }

        this.sort(cpr);
    }


    public void openTask(int index){
        ((MainActivity)context).openTask(index);
    }


    public static void setItemStyle(ViewGroup item, TaskListViewAdapter.STATE state){
        ToggleButton btn = (ToggleButton) item.getChildAt(0);
        // TextView tv = item.getChildAt(2);
        TextView tvElapsed = item.findViewById(R.id.timeElapsed);
        TextView tvTotal = item.findViewById(R.id.timeElapsedTotal);

        int color = 0;

        switch (state){
            case VIRGIN:
                btn.setChecked(true);
                btn.setBackgroundColor(getColor(R.color.listViewStateVirginBtnBackground));
                btn.setTextColor(getColor(R.color.listViewStateVirginBtnText));

                tvElapsed.setTextColor(getColor(R.color.listViewStateVirginElapsed));
                tvTotal.setTextColor(getColor(R.color.listViewStateVirginTotal));
            break;

            case RUNNING:
                btn.setChecked(true);
                btn.setBackgroundColor(getColor(R.color.listViewStateRunningBtnBackground));
                btn.setTextColor(getColor(R.color.listViewStateRunningBtnText));

                tvElapsed.setTextColor(getColor(R.color.listViewStateRunningElapsed));
                tvTotal.setTextColor(getColor(R.color.listViewStateRunningTotal));
            break;

            case DEFAULT:
            default:
                btn.setChecked(true);
                btn.setBackgroundColor(getColor(R.color.listViewStateDefaultBtnBackground));
                btn.setTextColor(getColor(R.color.listViewStateDefaultBtnText));

                tvElapsed.setTextColor(getColor(R.color.listViewStateDefaultElapsed));
                tvTotal.setTextColor(getColor(R.color.listViewStateDefaultTotal));
        }
    }

    public static void updateItemTime(ViewGroup item, long elapsed, long elapsedTotal, boolean odd){
        TimeFrac tfDelta = new TimeFrac(elapsed, odd);
        TimeFrac tfTotal = new TimeFrac(elapsedTotal, odd);
        // Log.i("TIME", Long.toString(delta));


        /*
        TextView tv = item.getChildAt(2);

        // tv.setText(c.convert(delta) + ", " + c.convert(elapsedTotal));
        tv.setText(tfTotal.toString() + "\n" + tfDelta.toString());
        */

        TextView tvElapsed = item.findViewById(R.id.timeElapsed);
        TextView tvTotal = item.findViewById(R.id.timeElapsedTotal);

        tvElapsed.setText(tfDelta.toString());
        tvTotal.setText(tfTotal.toString());
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.list_item2, parent, false);


        final Task t = this.tasks.get(position);
        long et = t.getElapsedTotal();

        if (t.isRunning()) {
            setItemStyle((ViewGroup) rowView, STATE.RUNNING);
            updateItemTime((ViewGroup) rowView, t.getElapsed(), et, this.tasks.isTickerOdd());
        }
        else {
            setItemStyle((ViewGroup) rowView, et > 0 ? STATE.DEFAULT : STATE.VIRGIN);
            updateItemTime((ViewGroup) rowView, 0, t.getElapsedTotal(), true);
        }


        ToggleButton buttonView = rowView.findViewById(R.id.button);

        buttonView.setOnClickListener(
            new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // boolean checked = btn.isChecked();
                    if (! t.isRunning()){
                        t.start();
                    }
                    else
                    {
                        t.stop();
                    }
                }
            }
        );


        TextView titleView = rowView.findViewById(R.id.title);

        titleView.setText(t.title);

        final int pos = position;
        titleView.setOnClickListener(
            new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    openTask(pos);
                }
            }
        );

        return rowView;
    }
}
