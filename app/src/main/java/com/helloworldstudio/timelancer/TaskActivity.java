package com.helloworldstudio.timelancer;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TaskActivity extends AppCompatActivity {
    public void log (String s){
        // Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        Log.d("debuglog", "TaskActivity: " + s);
    }


    boolean ready = false;


    private static int MODE_NEW = Actions.SAVE_TASK_FORM_MODE_NEW;
    private static int MODE_EDIT = Actions.SAVE_TASK_FORM_MODE_EDIT;

    private Toast toast;
    private int mode = -1;

    private void initToast() {
        // this.toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        this.toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
        // this.toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void toast (String text) {
        this.toast.setText(text);
        this.toast.show();
    }


    private TLService service;
    boolean serviceBound = false;
    private ServiceConnection serviceConnection;

    void initService(){
        log("initService()");
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder ibinder) {
                log("ServiceConnected");

                TLService.TLServiceBinder binder = (TLService.TLServiceBinder) ibinder;

                service = (TLService) binder.getService();
                serviceBound = true;


                initAll();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                log("ServiceDisconnected");

                service = null;
                serviceBound = false;
            }
        };

        log("bindService()");
        Intent intent = new Intent(this, TLService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    void cleanupService(){
        log("cleanupService()");

        if (serviceBound) {
            log("unbindService()");
            unbindService(serviceConnection);
            serviceBound = false;
        }
    }




    private int taskIndex;
    private Task task;

    public Task getTask(){
        return task;
    }
    public void setTask(Task task){
        this.task = task;
    }


    private Tasks.Listener tasksListener = null;


    void initTasksListener(){
        tasksListener = new Tasks.TickerListener(this) {
            @Override
            public void go(Tasks tasks, ArrayList<Task> tasksActive, boolean odd) { // loop thru all active tasks and update their list items
                if (ready){
                    TaskActivity a = TaskActivity.this;
                    Task t = a.getTask();

                    if (t != null && t.isRunning())
                    {
                        TextView ed = a.findViewById(R.id.elapsed);
                        TextView et = a.findViewById(R.id.elapsedTotal);

                        TimeFrac tfDelta = new TimeFrac(t.getElapsed(), odd);
                        TimeFrac tfTotal = new TimeFrac(t.getElapsedTotal(), odd);

                        ed.setText(tfDelta.toString("%h%:%m%:%s%"));
                        et.setText(tfTotal.toString("%h%:%m%:%s%"));
                    }
                }
            }
        };


        service.getTasks().addListener(tasksListener);
    }

    void cleanupTasksListener(){
        if (tasksListener != null) {
            service.getTasks().removeListener(tasksListener);
            tasksListener = null;
        }
    }


    void initAll(){
        // this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        this.taskIndex = intent.getIntExtra("index", -1); // getStringExtra(MainActivity.EXTRA_MESSAGE);

        mode = this.taskIndex != -1 ? MODE_EDIT : MODE_NEW;

        TextView el = findViewById(R.id.elapsedLabel);
        el.setVisibility(View.INVISIBLE);

        TextView ep = findViewById(R.id.elapsed);
        ep.setText("00:00:00");
        ep.setVisibility(View.INVISIBLE);


        TextView etl = findViewById(R.id.elapsedTotalLabel);
        etl.setVisibility(View.INVISIBLE);

        TextView ept = findViewById(R.id.elapsedTotal);
        ept.setText("00:00:00");
        ept.setVisibility(View.INVISIBLE);


        ToggleButton btn = findViewById(R.id.button);
        btn.setVisibility(View.INVISIBLE);

        Button rst = findViewById(R.id.btnReset);
        rst.setVisibility(View.INVISIBLE);


        if (mode == MODE_EDIT) {
            this.setTask(Tasks.getInstance().get(this.taskIndex));

            EditText et = findViewById(R.id.title);
            et.setText(this.task.title);


            TimeFrac tfElapsed = new TimeFrac(this.task.getElapsed(), false);
            TimeFrac tfTotal = new TimeFrac(this.task.getElapsedTotal(), false);

            el.setVisibility(View.VISIBLE);
            ep.setText(tfElapsed.toString("%h%:%m%:%s%"));
            ep.setVisibility(View.VISIBLE);

            etl.setVisibility(View.VISIBLE);
            ept.setText(tfTotal.toString("%h%:%m%:%s%"));
            ept.setVisibility(View.VISIBLE);
            /*
            int maxL = 18;
            String _title = t.title.length() > maxL ? t.title.substring(0, maxL) + "..." : t.title;
            setTitle("edit task \"" + _title + "\"");
            */

            int maxL = 38;
            String _title, _subtitle;

            /*
            _title = t.title.length() > maxL ? t.title.substring(0, maxL) + "..." : t.title;
            _title = "\"" + _title + "\"";
            _subtitle ="editing task...";
            */
            _subtitle = this.task.title.length() > maxL ? this.task.title.substring(0, maxL) + "..." : this.task.title;
            _subtitle = "\"" + _subtitle + "\"";
            _title = "Task";

            actionBarSetup();
            actionBarSet(_title, _subtitle);



            setStyle(this.task.isRunning() ? Task.STATE_RUNNING : Task.STATE_DEFAULT);


            btn.setVisibility(View.VISIBLE);

            btn.setOnClickListener(new View.OnClickListener(){

                private Task task;

                @Override
                public void onClick(View v) {
                    int state = task.toggle();
                    setStyle(state);
                }

                private View.OnClickListener init(Task task) {
                    this.task = task;
                    return this;
                }
            }.init(this.task));


            rst.setVisibility(View.VISIBLE);

            rst.setOnClickListener(new View.OnClickListener(){
                private Task task;

                @Override
                public void onClick(View v) {
                    doActionReset();
                }

                private View.OnClickListener init(Task task) {
                    this.task = task;
                    return this;
                }
            }.init(this.task));


            initTasksListener();
        }
        else if (mode == MODE_NEW) {
            int maxL = 38;
            String _title, _subtitle;

        /*
        _title = t.title.length() > maxL ? t.title.substring(0, maxL) + "..." : t.title;
        _title = "\"" + _title + "\"";
        _subtitle ="editing task...";
        */
            //_subtitle = this.task.title.length() > maxL ? this.task.title.substring(0, maxL) + "..." : this.task.title;
            //_subtitle = "\"" + _subtitle + "\"";
            _title = "New task";

            actionBarSetup();
            actionBarSet(_title, null);
        }


        ready = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        log("onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        initService(); // async calls initAll() on init
    }

    @Override
    protected void onDestroy() {
        log("onDestroy");

        cleanupTasksListener();
        cleanupService();

        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        //toast("ON RESTART");
        log("onRESTART");

        super.onRestart();

    }

    @Override
    protected void onStart() {
        log("onSTART");

        super.onStart();

        // TLApplication.setTaskActivity(this);
    }

    @Override
    protected void onStop() {
        log("onSTOP");

        super.onStop();

        // TLApplication.setTaskActivity(null);
    }

    @Override
    protected void onResume() {
        //toast("ON RESUME");
        log("onRESUME");

        super.onResume();

    }

    @Override
    protected void onPause() {
        //toast("ON PAUSE");
        log("onPAUSE");

        super.onPause();
    }



    public void setStyle(int state){
        // ToggleButton btn = (ToggleButton) item.getChildAt(0);
        Button btn = findViewById(R.id.button);
        // TextView tv = item.getChildAt(2);
        TextView ed = findViewById(R.id.elapsed);
        TextView et = findViewById(R.id.elapsedTotal);

        switch (state){
            case Task.STATE_RUNNING:
                btn.setText("STOP");

                // btn.setChecked(true);
                btn.setBackgroundColor(Color.parseColor("#aa0000"));
                btn.setTextColor(Color.parseColor("#ffffff"));

                ed.setTextColor(Color.parseColor("#aa0000"));
                et.setTextColor(Color.parseColor("#aa0000"));
                break;

            case Task.STATE_DEFAULT:
            default:
                btn.setText("START");

                //btn.setChecked(false);
                btn.setBackgroundColor(Color.parseColor("#999999"));
                btn.setTextColor(Color.parseColor("#cccccc"));

                ed.setTextColor(Color.parseColor("#cccccc"));
                et.setTextColor(Color.parseColor("#cccccc"));
        }
    }

    private void doActionSave(){
        EditText et = findViewById(R.id.title);
        String title = et.getText().toString();

        Map<String, String> data = new HashMap<>();
        data.put("title", title);

        Actions.saveForm(this, this.taskIndex, this.task, data, mode, new Actions.Listener(){
            @Override
            public void onDone(Map<String, String> data) {
                getBackToMain((mode == MODE_EDIT) ? "save" : "new", data);
            }
        });
    }


    private void doActionDelete() {
        Actions.delete(this, this.taskIndex, this.task, new Actions.Listener(){
            @Override
            public void onDone(Map<String, String> data) {
            getBackToMain("delete", data);
            }
        });
    }

    private void doActionReset() {
        Actions.reset(this, this.taskIndex, this.task, new Actions.Listener(){
            @Override
            public void onDone(Map<String, String> data) {
                // Reset style
                setStyle(Task.STATE_DEFAULT);

                // Reset time labels

                TextView ep = findViewById(R.id.elapsed);
                ep.setText("00:00:00");

                TextView ept = findViewById(R.id.elapsedTotal);
                ept.setText("00:00:00");
            }
        });
    }

    public void getBackToMain(String action, Map<String, String> data) {
        Intent i = new Intent();
        i.putExtra("action", action);

        i.putExtra("index", this.taskIndex);
        i.putExtra("title", data.get("title"));

        setResult(Activity.RESULT_OK, i);

        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.task_activity_menu, menu);

        MenuItem itemDelete = menu.findItem(R.id.action_delete);
        if (mode == MODE_NEW) itemDelete.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                doActionSave();
                break;

            case R.id.action_delete:
                doActionDelete();
                break;

            default:
                return super.onOptionsItemSelected(item);

        }

        return super.onOptionsItemSelected(item);
    }

    ActionBar abar = null;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // ActionBar ab = getActionBar();
            abar = getSupportActionBar();
        }

    }

    private void actionBarSet (String title, String subtitle){
        if (abar != null) {
            abar.setTitle(title);
            if (subtitle != null) abar.setSubtitle(subtitle);
        }

    }
}
