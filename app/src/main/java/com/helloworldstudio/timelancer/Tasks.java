package com.helloworldstudio.timelancer;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by pe60 on 30.06.2017.
 */


public class Tasks {
    public void log (String s){
        // Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        Log.d("debuglog", "Tasks: " + s);
    }

    public static class Totals{
        public int count;
        public long elapsed;
    }


    public interface Listener {
        // void onSchedule(int index, Task t, long delta, long elapsedTotal);
        void onStart(int index, Task t);
        void onStop(int index, Task t);
        void onUpdate(int index, Task t);
        void onDelete(int index, Task t);
        void onCreate(int index, Task t);
        void onTicker(Tasks self, ArrayList<Task> tasks, boolean odd);
        void onReset(int index, Task t);
    }

    public static class ListenerRunnable implements Runnable {
        int             index;
        Task            task;

        public void go(){
            // override this!
        }

        @Override
        public void run() {
            go();
        }

        public Runnable init(int index, Task t){
            this.index = index;
            this.task  = t;

            return this;
        }
    }

    public static class ListenerTickerRunnable implements Runnable {
        Tasks           tasks;
        ArrayList<Task> tasksActive;
        boolean         odd;

        public void go(){
            // override this!
        }

        @Override
        public void run() {
            go();
        }

        public Runnable init(Tasks tasks, ArrayList<Task> tasksActive, boolean odd){
            this.tasks = tasks;
            this.tasksActive = tasksActive;
            this.odd = odd;

            return this;
        }
    }

    /**
     * Helper listener for ticker event only
     */
    public static class TickerListener implements Listener {
        Context context;

        public TickerListener(Context c){
            context = c;
        }

        public void go(Tasks tasks, ArrayList<Task> tasksActive, boolean odd){
            // override this!
        }

        @Override
        public void onTicker(Tasks tasks, ArrayList<Task> tasksActive, boolean odd) {
            ((Activity)context).runOnUiThread(new ListenerTickerRunnable() {
                @Override
                public void go() { // loop thru all active tasks and update their list items
                    TickerListener.this.go(tasks, tasksActive, odd);
                }
            }.init(tasks, tasksActive, odd));
        }

        // Stubs for unused events
        @Override
        public void onStart (int index, Task t) {}
        @Override
        public void onStop  (int index, Task t) {}
        @Override
        public void onUpdate(int index, Task t) {}
        @Override
        public void onDelete(int index, Task t) {}
        @Override
        public void onCreate(int index, Task t) {}
        @Override
        public void onReset(int index, Task t) {}
    }


    private DBAdapter dba;
    private ArrayList<Task> tasks;
    private ArrayList<Task> tasksActive;

    // private TasksListener tasksListener;
    private List<Tasks.Listener> tasksListeners;

    static private Tasks instance;

    private boolean single = true; // inital state


    private ScheduledExecutorService tickerExecutor = null;
    private ScheduledFuture<?>       tickerFuture   = null;
    private long     tickerDelay  = 0;
    private long     tickerPeriod = 500;
    private TimeUnit tickerUnit   = TimeUnit.MILLISECONDS;
    private boolean  tickerOdd = false;


    private Tasks(DBAdapter dba){
        this.dba = dba;
        this.tasks       = new ArrayList<>();
        this.tasksActive = new ArrayList<>();

        this.tasksListeners = new ArrayList<>();

        initTicker();
    }

    static public Tasks create(DBAdapter dba){
        instance = new Tasks(dba);
        return instance;
    }

    static public Tasks getInstance(){
        return instance;
    }


    // public void setListener(TasksListener l) { tasksListener = l; }

    public void addListener(Tasks.Listener l) {
        tasksListeners.add(l);
    }

    public void removeListener(Tasks.Listener l){
        tasksListeners.remove(l);
    }


    public void setSingle(boolean single){
        this.single = single;
    }

    public boolean isSingle(){
        return single;
    }


    public int indexOf(Task t)    {
        return tasks.indexOf(t);
    }

    public boolean isTickerOdd(){
        return tickerOdd;
    }

    private long startedAt = 0;

    private void onTicker(){
        long cur = SystemClock.elapsedRealtime();

        log(String.format("onTicker - %d, %.1f", cur, (cur - startedAt) / 1000.0));

        tickerOdd = !tickerOdd;

        synchronized (tasksActive) {
            for (Tasks.Listener l : tasksListeners) {
                l.onTicker(this, tasksActive, !tickerOdd);
            }
        }
    }

    private void initTicker(){
        this.tickerExecutor = Executors.newSingleThreadScheduledExecutor();
    }

    private void resetTicker(){
        tickerOdd = false;
    }

    private void startTicker(){
        if (tickerFuture == null) {//  throw new RuntimeException("trying to start ticker but ticker is already running");
            resetTicker();

            startedAt = SystemClock.elapsedRealtime();

            tickerFuture = this.tickerExecutor.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    onTicker();
                }
            }, tickerDelay, tickerPeriod, tickerUnit);
        }
    }

    private void stopTicker(){
        if (tickerFuture != null) {
            tickerFuture.cancel(false);
            tickerFuture = null;
        }
    }

    private boolean hasActiveTasks(){
        return tasksActive.size() > 0;
    }

    public int getActiveTaskCount(){
        return tasksActive.size();
    }

    public ArrayList<Task.Info> enumActiveTasksInfo(){
        ArrayList<Task.Info> infos = new ArrayList<>();

        for (Task t : tasksActive) {
            infos.add(t.getInfo());
        }

        return infos;
    }

    private void addActive(Task t){
        synchronized (tasksActive) {
            tasksActive.add(t);
        }

        startTicker();
    }

    private void removeActive(Task t){
        synchronized (tasksActive) {
            tasksActive.remove(t);
        }

        if (! hasActiveTasks()) stopTicker();
    }

    private void initTask(Task t){
        t.setDBA(this.dba);

        t.addListener(new Task.Listener () {
            private ArrayList<Task> tasks;

            /*
            public void onSchedule(Task t, long delta, long elapsedTotal)
            {
                tasksListener.onSchedule(tasks.indexOf(t), t, delta, elapsedTotal);
            };
            */

            public void onStart(Task t)
            {
                if (isSingle()) stopAll(t);


                addActive(t);

                for (Tasks.Listener l : tasksListeners) {
                    l.onStart(tasks.indexOf(t), t);
                }
            }

            public void onStop(Task t)
            {
                removeActive(t);

                for (Tasks.Listener l : tasksListeners) {
                    l.onStop(tasks.indexOf(t), t);
                }
            }

            public void onUpdate(Task t)
            {
                for (Tasks.Listener l : tasksListeners) {
                    l.onUpdate(tasks.indexOf(t), t);
                }
            }

            public void onDelete(Task t)
            {
                int wasIndex = tasks.indexOf(t);
                tasks.remove(t);

                for (Tasks.Listener l : tasksListeners) {
                    l.onDelete(wasIndex, t);
                }
            }
            /*
            public void onCreate(Task t)
            {
                tasksListener.onCreate(tasks.indexOf(t), t);
            };*/

            public void onReset(Task t)
            {
                for (Tasks.Listener l : tasksListeners) {
                    l.onReset(tasks.indexOf(t), t);
                }
            }

            public Task.Listener init(ArrayList<Task> tasks){
                this.tasks = tasks;
                return this;
            }
        }.init(this.tasks));

        this.tasks.add(t);
    }


    private Task add(int id, String title, String ts, int state, long elapsedTotal){
        Task t = new Task(id, title, ts, state, elapsedTotal);
        initTask(t);

        return t;
    }

    private Task add(String id, String title, String ts, String state, String elapsedTotal){
        Task t = new Task(id, title, ts, state, elapsedTotal);
        initTask(t);

        return t;
    }

    private Task add(DBContract.TaskRow tr) {
        Task t = new Task(tr);
        initTask(t);

        return t;
    }


    // contains stubs, not used, needs review!
    public void load(String[][] list, boolean add) {
        if (!add) this.tasks.clear(); // TODO: correctly stop/destroy/unbind all tasks

        for (String[] row : list) {
            String ts = ((Long)SystemClock.elapsedRealtimeNanos()).toString(); // override row[1]
            String state = "0"; // override row[2]
            String elapsedTotal = "0";

            // 0: ID, 1: TITLE
            add(row[0], row[1],  ts, state, elapsedTotal);
        }

    }
    public void load(String[][] list) { load(list, true); }

    public void load(ArrayList<DBContract.TaskRow> trs, boolean add) {
        if (!add) this.tasks.clear(); // TODO: correctly stop/destroy/unbind all tasks

        for (DBContract.TaskRow tr : trs){
            add(tr);
        }
    }
    public void load(ArrayList<DBContract.TaskRow> trs) { load(trs, true); }


    public Task create(String title, int state){
        Task t = Task.create(dba, title ,state);
        initTask(t);

        for (Tasks.Listener l : tasksListeners) {
            l.onCreate(tasks.indexOf(t), t);
        }

        return t;
    }


    public Task get(int index)
    {
        return this.tasks.get(index);
    }

    public Task findById(int id)
    {
        Task r = null;

        for (Task t : this.tasks) {
            if (t.id == id){
                r = t;
                break;
            }
        }

        return r;
    }


    public List<Task> asList(){
        return this.tasks;
    }

    public int getCount() {
        return this.tasks.size();
    }

    public long getElapsedTotal() {
        long total = 0;

        for (Task t : this.tasks) total += t.getElapsedTotal();

        return total;
    }

    public Tasks.Totals getTotals(){
        return new Tasks.Totals(){{
            Tasks ts = Tasks.this;

            count   = ts.getCount();
            elapsed = ts.getElapsedTotal();
        }};
    }



    public void stopAll(Task exceptTask){
        synchronized (tasks) {
            for (Task t : tasks) {
                if (t != exceptTask)
                    t.stop();
            }
        }

    }

    public void resetAll(){
        synchronized (tasks) {
            for (Task t : tasks) {
                t.reset();
            }
        }

    }
}
