package com.helloworldstudio.timelancer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by pe60 on 28.06.2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context) {
        super(context, DBContract.DATABASE_NAME, null, DBContract.DATABASE_VERSION);
    }


    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBContract.TaskTable.SQL_CREATE);
        db.execSQL(DBContract.TaskTable.SQL_INSERT_INIT);
    }

    // Method is called during an upgrade of the database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // db.execSQL(DBContract.TaskTable.SQL_DELETE);

        // continious upgrade
        switch (newVersion)
        {
            case 1:
            case 2:
                db.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT 0", DBContract.TaskTable.TABLE_NAME, DBContract.TaskTable.COLUMN_NAME_ELAPSED_TOTAL));
        }
    }

    /*
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    */

}
