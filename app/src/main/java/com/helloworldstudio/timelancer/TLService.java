package com.helloworldstudio.timelancer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
// import android.support.v7.app.NotificationCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


/**
 * Created by pe60 on 23.07.2017.
 */



public class TLService extends TLAbstractService {
    static public void log (String s){
        // Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        Log.d("debuglog", "TLService: " + s);
    }

    // private static TLService self = null;

    public TLService(){
        log("construct");

        //self = this;
    }
    /*
    public static TLService getInstance(){
        return self;
    }
    */



    public void showNotification()
    {
        int runningTaskNumber = 3;

        Class<MainActivity> ResultActivityClass = MainActivity.class;

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_tl_round)
                .setContentTitle("Timelancer")
                .setContentText(String.format("Running %s task(s)", runningTaskNumber))
                .setNumber(runningTaskNumber)
                .setTicker(String.format("Running %s task(s)", runningTaskNumber));


        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        String[] events = {"task one", "task two", "task four", "task five", "task N"};// new String[6];
        // Sets a title for the Inbox in expanded layout
        inboxStyle.setBigContentTitle(String.format("Running %s task(s)", runningTaskNumber) + ":");

        // Moves events into the expanded layout
        for (String e : events) {
            inboxStyle.addLine(e);
        }
        // Moves the expanded layout object into the notification object.
        mBuilder.setStyle(inboxStyle);


        Intent resultIntent = new Intent(this, ResultActivityClass);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(ResultActivityClass);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        Notification nn = mBuilder.build();


        int mNotificationId = 1;

        NotificationManager mgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mgr.notify(mNotificationId, nn);
    }

    /*
    private ScheduledExecutorService tickerExecutor = null;
    private ScheduledFuture<?>       tickerFuture   = null;
    private long     tickerDelay  = 0;
    private long     tickerPeriod = 3000;
    private TimeUnit tickerUnit   = TimeUnit.MILLISECONDS;


    int tickerCount = 0;
    private void onTicker(){
        tickerCount++;
        Notification notification = getFgNotificationBuilder("Timelancer service", "sched run " + tickerCount, "sched run " + tickerCount).build();

        startForeground(fgNotificationId, notification);
    }

    private void initTicker(){
        this.tickerExecutor = Executors.newSingleThreadScheduledExecutor();
    }


    private void startTicker(){
        if (tickerFuture == null) {//  throw new RuntimeException("trying to start ticker but ticker is already running");

            tickerFuture = this.tickerExecutor.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    onTicker();
                }
            }, tickerDelay, tickerPeriod, tickerUnit);
        }
    }

    private void stopTicker(){
        if (tickerFuture != null) {
            tickerFuture.cancel(false);
            tickerFuture = null;
        }
    }
    */


    int fgNotificationId = 2;

    private int getNotificationIcon() {
        return R.mipmap.ic_logo;
        // boolean monochromeIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        // return monochromeIcon ? R.drawable.ic_running : R.mipmap.ic_logo;
        // return monochromeIcon ? R.drawable.ic_running : R.mipmap.ic_launcher_tl_round;
    }

    private int getNotificationColor(){
        // int color = Color.argb(50,100,0,0);
        // int color = R.color.ic_launcher_background;
        // int color = Color.parseColor("#00AEEF");
        int color = getResources().getColor(R.color.brandBlue);

        return color;
    }

    private NotificationCompat.Builder getFgNotificationBuilder(String title, String msg, String ticker){
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);

        return new NotificationCompat.Builder(this, TLApplication.ChannelInfo.id)
            .setContentTitle(title)
            .setContentText(msg)
            .setTicker(ticker)
            // .setSmallIcon(R.mipmap.ic_launcher_tl_round)
            // .setSmallIcon(R.drawable.ic_launcher_tl_round_small
            // .setSmallIcon(R.drawable.ic_running)
            .setSmallIcon(getNotificationIcon())
            // .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), getNotificationIcon()))
            .setContentIntent(pendingIntent)
            .setPriority(TLApplication.ChannelInfo.priority)
            .setColorized(true)
            .setColor(getNotificationColor())

            .setSound(Uri.EMPTY);
    }

    private void startFg(String msg){
        Notification notification = getFgNotificationBuilder("Timelancer", msg, msg).build();

        startForeground(fgNotificationId, notification);


        // startTicker();
    }

    private void stopFg(){
        // stopTicker();


        stopForeground(true);
    }


    public void dispatch(Bundle b) throws InterruptedException {
        int startId   = b.getInt("startId");
        String action = b.getString("action");

        switch (action) {
            case "start":
                  // Thread.currentThread().sleep(5000);
                  Looper.getMainLooper().getThread().sleep(5000);
                break;
            case "notify":
                showNotification();
                break;
            case "startfg":
                startFg("onclick");
                break;
            case "stopfg":
                stopFg();
                break;
            case "stop":
                stopSelf(startId);
                break;
            default:
                // Toast.makeText(this, "service started", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log("service onStartCommand");

        String action = intent.getStringExtra("action");
        log("action: " + action);

        /*
        switch (action){
            case "go":
                try{

                    // Thread.currentThread().sleep(5000);
                    Looper.getMainLooper().getThread().sleep(5000);
                }
                catch (InterruptedException e){

                }
                break;
            case "stop":
                stopSelf(startId);
                break;
        }
       */

        Bundle b = new Bundle();
        b.putInt("startId",startId);
        b.putString("action",action);

        postToThread(b);



        // If we get killed, after returning from here, restart
        return START_STICKY;

    }



    private DBAdapter dba;

    private void initDBA(){
        try {
            DBAdapter dba = new DBAdapter(getApplicationContext());

            dba.open();

            this.dba = dba;
        }
        catch(SQLException e) {
            String msg = e.getMessage();
            Log.e("SQL", msg);

            throw e;
        }
    }

    private void cleanupDBA(){
        dba.close();
    }

    public DBAdapter getDBA(){
        return dba;
    }


    private Tasks tasks;
    private Tasks.Listener tasksListener = null;

    private void startFgShowRunningTasks(){
        ArrayList<Task.Info> atis = tasks.enumActiveTasksInfo();

        String firstTaskTitle = (atis.get(0)).title;
        int restCount = atis.size() - 1;

        String template = "running \"%s\"" + (restCount > 0 ? "  +%d more task" + (restCount > 1 ? "s" : "") : "");
        String message = Utils.format(template, firstTaskTitle, restCount);

        startFg(message);
    }

    private void initTasks(){
        tasks = Tasks.create(dba);

        ArrayList<DBContract.TaskRow> trs = dba.getAllTasks();
        tasks.load(trs);


        tasksListener = new Tasks.Listener() {
            public void onStart(int index, Task t) {
                // Make service started
                Intent intent = new Intent(getApplicationContext(), TLService.class);
                intent.putExtra("action", "default");
                startService(intent);

                startFgShowRunningTasks();
            }

            public void onStop(int index, Task t) {
                int c = tasks.getActiveTaskCount();
                if (c > 0) {
                    startFgShowRunningTasks();
                }
                else {
                    stopFg();
                    stopSelf();
                }
            }

            public void onUpdate(int index, Task t) {
                if(t.isRunning()) {
                    startFgShowRunningTasks();
                }
            };

            public void onDelete(int index, Task t){};
            public void onCreate(int index, Task t){};
            public void onTicker(Tasks self, ArrayList<Task> tasks, boolean odd){};
            public void onReset(int index, Task t) {}
        };

        tasks.addListener(tasksListener);
    }

    void cleanupTasks(){
        tasks.removeListener(tasksListener);
    }

    public Tasks getTasks(){
        return tasks;
    }


    @Override
    public void onCreate() {
        log("onCreate");

        super.onCreate();

        // initTicker(); // debug

        initDBA();
        initTasks();
    }

    @Override
    public void onDestroy() {
        log("onDestroy");

        // stopTicker();

        cleanupDBA();
        cleanupTasks();

        super.onDestroy();
    }
}
