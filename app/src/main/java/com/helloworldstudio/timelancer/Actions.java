package com.helloworldstudio.timelancer;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

public class Actions {
    // Action listener
    public static class Listener {
        public void onDo   (Map<String, String> data, String... params) {}
        public void onDone (Map<String, String> data) {}
    }

    private static AlertDialog.Builder getAlertDialogBuilder(Context ctx, String title, String message){
        return Utils.getAlertDialogBuilder(ctx, title, message)
            .setIcon(android.R.drawable.ic_dialog_alert);
    }

    private static AlertDialog.Builder getNoAlertDialogBuilder(Context ctx, String title, String message){
        return Utils.getAlertDialogBuilder(ctx, title, message)
            .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // NO
                }
            });
    }

    // Dialog onClick listener helper class
    private static class DICListener implements DialogInterface.OnClickListener {
        protected Context ctx;
        protected int taskIndex;
        protected Task task;
        protected Listener lnr;

        @Override
        public void onClick(DialogInterface dialog, int which) {
            // override on definition
        }

        // call to init on definition
        public DICListener init(Context ctx, int taskIndex, Task task, Listener lnr) {
            this.ctx = ctx;
            this.taskIndex = taskIndex;
            this.task = task;
            this.lnr = lnr;
            return this;
        }
    }


    abstract private static class AsyncTask extends android.os.AsyncTask<String, Integer, Long> {
        String DTAG = "Actions.AsyncTask";

        protected ProgressDialog mDialog = null;
        protected String progressMessage = "Executing...";

        protected Context ctx;
        protected int taskIndex;
        protected Task task;
        protected Map<String, String> data;
        protected Listener lnr;

        public AsyncTask(Context ctx, int index, Task task, Map<String, String> data, Listener lnr) {
            this.ctx = ctx;
            this.taskIndex = index;
            this.task = task;
            this.data = data;
            this.lnr = lnr;
        }

        @Override
        protected void onPreExecute() {
           //  mDialog = ProgressDialog.show(this.ctx, "", "Deleting...", true, false);
            if (progressMessage != null && !progressMessage.isEmpty()) showProgess(progressMessage);
        }

        @Override
        protected void onPostExecute(Long result) {
            if (mDialog != null) mDialog.dismiss();

            lnr.onDone(data); // return from task
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if (mDialog != null) mDialog.setProgress(values[0]);

            Log.v(DTAG, "onProgressUpdate(): " + values[0]);
        }

        @Override
        protected Long doInBackground(String... params) {
            lnr.onDo(data, params); // do task

            return null; // default result, call return super.doInBackground() if no customization needed
        }

        protected void showProgess(String message, String title) {
            mDialog = ProgressDialog.show(ctx, title, message, true, false);
        }
        protected void showProgess(String message) {
            mDialog = ProgressDialog.show(ctx, "", message, true, false);
        }
    }


    /**
     *  Delete task  Task
     */

    private static class DeleteTaskTask extends Actions.AsyncTask {
        String DTAG = "DeleteTaskTask";
        String progressMessage = "Deleting...";

        public DeleteTaskTask(Context ctx, int index, Task task, Map<String, String> data, Listener lnr) {
            super(ctx, index, task, data, lnr);
        }

        @Override
        protected Long doInBackground(String... params) {
            this.task.delete();

            return super.doInBackground(params);
        }
    }

    public static void delete(Context ctx, int taskIndex, Task task, Listener lnr){
        getNoAlertDialogBuilder(ctx, "Delete task", "Really delete this task?")
            .setPositiveButton("YES", new DICListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Map<String, String> data = new HashMap<>(); //reserved for future use
                    // data.put("title", title);

                    new Actions.DeleteTaskTask(ctx, this.taskIndex, this.task, data, lnr).execute();
                }
            }.init(ctx, taskIndex, task, lnr))
            .show();
    }


    /**
     *  Save task form Task
     */

    public static int SAVE_TASK_FORM_MODE_NEW = 1;
    public static int SAVE_TASK_FORM_MODE_EDIT = 2;

    private static class SaveTaskFormTask extends Actions.AsyncTask {
        String DTAG = "SaveTaskFormTask";

        public SaveTaskFormTask(Context ctx, int index, Task task, Map<String, String> data, Listener lnr) {
            super(ctx, index, task, data, lnr);
        }

        @Override
        protected Long doInBackground(String... params) {
            int mode = Integer.parseInt(params[0]); // convert back to int

            if (mode == SAVE_TASK_FORM_MODE_EDIT)
                this.task.updateTitle(this.data.get("title"));
            else if (mode == SAVE_TASK_FORM_MODE_NEW)
                Tasks.getInstance().create(this.data.get("title"), 0);

            return super.doInBackground(params);
        }
    }

    public static void saveForm(Context ctx, int taskIndex, Task task, Map<String, String> data, int mode, Listener lnr){
        new Actions.SaveTaskFormTask(ctx, taskIndex, task, data, lnr).execute(
            String.valueOf(mode) // convert to String
        );
    }


    /**
     * Reset tast  Task
     */

    private static class ResetTaskTask extends Actions.AsyncTask {
        String DTAG = "ResetTaskTask";
        String progressMessage = "Deleting...";

        public ResetTaskTask(Context ctx, int index, Task task, Map<String, String> data, Listener lnr) {
            super(ctx, index, task, data, lnr);
        }

        @Override
        protected Long doInBackground(String... params) {
            this.task.reset();

            return super.doInBackground(params);
        }
    }

    public static void reset(Context ctx, int taskIndex, Task task, Listener lnr){
        getNoAlertDialogBuilder(ctx, "Reset task", "Really reset this task?")
            .setPositiveButton("YES", new DICListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Map<String, String> data = new HashMap<>(); //reserved for future use
                    // data.put("title", title);

                    new Actions.ResetTaskTask(ctx, taskIndex, task, data, lnr).execute();
                }
            }.init(ctx, taskIndex, task, lnr))
            .show();
    }
}
