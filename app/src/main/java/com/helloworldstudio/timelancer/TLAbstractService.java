package com.helloworldstudio.timelancer;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;

/**
 * Created by pe60 on 27.07.2017.
 */

public abstract class TLAbstractService extends Service {
    static public void log (String s){
        // Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        Log.d("debuglog", "TLAbstractService: " + s);
    }


    public class TLServiceBinder extends Binder {
        TLAbstractService getService() {
            return TLAbstractService.this;
        }
    }

    private IBinder binder = new TLServiceBinder();


    @Override
    public IBinder onBind(Intent intent) {
        log("onBind");

        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        log("onUnbind");
        return false; // must return true in order to have next onBind/onRebind called
    }

    @Override
    public void onRebind(Intent intent) {
        log("onRebind");
        super.onRebind(intent);
    }

    private HandlerThread  looperThread;
    private ServiceHandler serviceHandler;


    abstract public void dispatch(Bundle b) throws InterruptedException;


    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.
            try {
                // Thread.sleep(5000);
                dispatch(msg.getData());

            } catch (InterruptedException e) {
                // Restore interrupt status.
                Thread.currentThread().interrupt();
            }

            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
            // stopSelf(msg.arg1);
        }
    }


    protected void postToThread(Bundle bundle){
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = serviceHandler.obtainMessage();

        msg.setData(bundle);

        serviceHandler.sendMessage(msg);
    }

    private void prepareThread(){
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        looperThread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
        looperThread.start();

        serviceHandler = new ServiceHandler(looperThread.getLooper());
    }

    private void quitThread(){
        looperThread.quit();
    }

    @Override
    public void onCreate() {
        TLAbstractService.log("onCreate");

        prepareThread();
    }

    @Override
    public void onDestroy() {
        TLAbstractService.log("onDestroy");

        quitThread();
    }
}
