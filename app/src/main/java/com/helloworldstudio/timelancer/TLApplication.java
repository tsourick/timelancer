package com.helloworldstudio.timelancer;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by pe60 on 30.06.2017.
 */

public class TLApplication extends Application {
    final public static String LOG_TAG = "debuglog";

    public static void log (String s){
        Log.d(LOG_TAG, s);
    }


    private static Context context;

    public static class ChannelInfo {
        static String id = "default",
                name = "default",
                description = "default notification channel";
        static int importance = NotificationManager.IMPORTANCE_LOW;
        static int priority = NotificationCompat.PRIORITY_LOW;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager nm = getSystemService(NotificationManager.class);

            NotificationChannel channel = new NotificationChannel(ChannelInfo.id, ChannelInfo.name, ChannelInfo.importance);
            channel.setDescription(ChannelInfo.description);

            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            nm.createNotificationChannel(channel);
        }
    }

    public void onCreate() {
        super.onCreate();

        TLApplication.context = getApplicationContext();

        createNotificationChannel();

        log("App Version: " + getVersion());
    }

    public static Context getAppContext() {
        return TLApplication.context;
    }


    public static TaskActivity taskActivity = null;

    public static void setTaskActivity(TaskActivity taskActivity) {
        TLApplication.taskActivity = taskActivity;
    }


    public String getVersion () {
        return this.getResources().getString(R.string.app_version);
    }
}
