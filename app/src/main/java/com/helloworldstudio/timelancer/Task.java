package com.helloworldstudio.timelancer;

import android.database.Cursor;
import android.os.SystemClock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pe60 on 30.06.2017.
 */

public class Task {

    public interface Listener {
        // void onSchedule(Task t, long delta, long elapsedTotal);
        void onStart(Task t);
        void onStop(Task t);
        void onUpdate(Task t);
        void onDelete(Task t);
        // void onCreate(Task t);
        void onReset(Task t);
    }


    public static class Info {
        public int     id;
        public String  title;
        public String  ts;
        public int     state;
        public long    elapsedTotal;

        public long lastStart = 0;
    }


    private DBAdapter dba;

    public int id;
    public String title;
    public String ts;
    public int state;
    public long elapsedTotal;

    public long lastStart = 0;

    public final static int STATE_DEFAULT = 0;
    public final static int STATE_RUNNING = 1;

    /*
    private ScheduledExecutorService scheduler = null;
    private ScheduledFuture<?> future = null;
    */

    private List<Task.Listener> taskListeners;


    public Info getInfo(){
        return new Info(){{
            Task t = Task.this;

            id           = t.id;
            title        = t.title;
            ts           = t.ts;
            state        = t.state;
            elapsedTotal = t.elapsedTotal;

            lastStart    = t.lastStart;
        }};
    }


    public Task(int id, String title, String ts, int state, long elapsedTotal){
        this.id = id;
        this.title = title;
        this.ts = ts;
        this.state = state;
        this.elapsedTotal = elapsedTotal;

        // this.scheduler = Executors.newSingleThreadScheduledExecutor();
        // this.scheduler = Executors.newScheduledThreadPool(1);


        this.taskListeners = new ArrayList<>();
    }

    public Task(String id, String title, String ts, String state, String elapsedTotal){
        this(Integer.parseInt(id), title, ts, Integer.parseInt(state), Long.parseLong(elapsedTotal));
    }

    public Task(DBContract.TaskRow tr){
        this(tr.id, tr.title, tr.ts, tr.state, tr.elapsedTotal);
    }


    public void setDBA(DBAdapter dba){
        this.dba = dba;
    }


    public void addListener(Task.Listener l) {
        taskListeners.add(l);
    }

    public void removeListener(Task.Listener l){
        taskListeners.remove(l);
    }


    // Returns last delta
    public long getElapsed(){
        return lastStart > 0 ? (SystemClock.elapsedRealtimeNanos() - lastStart) : -1;
    }

    public long getElapsedTotal(){
        long elapsed = getElapsed(); // -1 if stopped, last elasped if running
        return elapsedTotal + (elapsed > 0 ? elapsed : 0);
    }


    static public Task create(DBAdapter dba, String title, int state){
        state = STATE_DEFAULT; // override state

        long id = dba.insertTask(title, state); // TODO enclose in try...catch?

        if (id < 0) throw new RuntimeException("could not insert task into DB");

        Cursor s = dba.getTask(id);

        if (s == null) throw new RuntimeException("could not find just inserted task in DB");

        s.moveToFirst();
        DBContract.TaskRow tr = dba.getTaskRow(s);
        s.close();

        return new Task(tr);
    }

    public void updateTitle(String title) {
        dba.updateTaskTitle(this.id, title); // TODO enclose in try...catch?

        this.title = title;

        for (Task.Listener l : taskListeners) {
            l.onUpdate(this);
        }
    }

    public void delete() {
        if (this.isRunning()) this.stop();


        dba.deleteTask(this.id); // TODO enclose in try...catch?

        for (Task.Listener l : taskListeners) {
            l.onDelete(this);
        }
    }

    /*
    private void onSchedule(){
        long elapsed = getElapsed();
        long elapsedTotal = getElapsedTotal();

        for (TaskListener l : taskListeners) {
            l.onSchedule(this, elapsed, elapsedTotal);
        }
    }
    */

    public void start (){
        stop();


        // TODO: enclose with try...except an return boolean result

        lastStart = SystemClock.elapsedRealtimeNanos();
        state = STATE_RUNNING;

        /*
        future = this.scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                onSchedule();
            }
        }, 0, 500, TimeUnit.MILLISECONDS);
        */

        dba.updateTaskState(this.id, DBContract.TaskTable.COLUMN_VALUE_STATE_RUNNING);

        for (Task.Listener l : taskListeners) {
            l.onStart(this);
        }
    }

    public boolean stop (){
        boolean r;

        if (lastStart != 0) {
            /*
            future.cancel(false);
            future = null;
            */

            elapsedTotal += getElapsed();

            lastStart = 0;
            state = STATE_DEFAULT;

            dba.updateTaskState(this.id, DBContract.TaskTable.COLUMN_VALUE_STATE_DEFAULT, elapsedTotal);

            for (Task.Listener l : taskListeners) {
                l.onStop(this);
            }

            r = true;
        }
        else{
            r = false;
        }

        return r;
    }

    public int toggle(){
        if (this.isRunning()) stop(); else start();
        return state;
    }

    public void reset(){
        if (this.isRunning()) this.stop();

        state = 0;
        elapsedTotal = 0;
        lastStart = 0;
        ts = "0000-00-00 00:00:00";

        dba.resetTask(this.id);

        for (Task.Listener l : taskListeners) {
            l.onReset(this);
        }
    }

    public boolean isRunning(){
        return lastStart != 0;
    }
}
