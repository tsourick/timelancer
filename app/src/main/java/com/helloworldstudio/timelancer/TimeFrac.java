package com.helloworldstudio.timelancer;

import java.util.concurrent.TimeUnit;

/**
 * Created by pe60 on 17.07.2017.
 */

public class TimeFrac{
    public long h;
    public long m;
    // long s;
    // long i;
    public double s; //rounded to 1 fractional char
    public boolean odd;

    public double round (long i){
        return (double)Math.round((double)i/100)/10;
    }

    public String toString(String f)
    {
        f = ! odd ? "%h%:%m%:%s%" : "%h% %m% %s%"; // override

        f = f.replace("%h%", Utils.format("%02d", h));
        f = f.replace("%m%", Utils.format("%02d", m));
        // f = f.replace("%s%", String.format("%04.1f", s));
        f = f.replace("%s%", Utils.format("%02d", (long)s));

        // if (hasTick()) f += " *";
        // if (! odd) f += " *";

        return f;
    }
    public String toString(){
        return toString("%h%:%m%:%s%");
    }


    public boolean hasTick(){
        double f = (s - Math.floor(s)) * 10;
        return f < 5; // 0..4 has frac, 5..9 hasn't
    }

    public TimeFrac (long delta, boolean odd){

        // Log.i("TIME", Long.toString(delta));


        h = TimeUnit.NANOSECONDS.toHours(delta);
        m = TimeUnit.NANOSECONDS.toMinutes(delta) % TimeUnit.HOURS.toMinutes(1);
        // s = TimeUnit.NANOSECONDS.toSeconds(delta) % TimeUnit.NANOSECONDS.toSeconds(1);
        long i = TimeUnit.NANOSECONDS.toMillis(delta) % TimeUnit.MINUTES.toMillis(1);
        s = round(i);


        this.odd = odd;
        // Log.i("TIME", String.format("%d", i));
    }
}
