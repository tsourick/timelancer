package com.helloworldstudio.timelancer;

import android.annotation.TargetApi;
// import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.widget.Button;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {
    public void log (String s){
        TLApplication.log("MainActivity: " + s);
    }


    public ListView listView;
    public String[] listItems = {"Roman","Alex","Ramiro","David", "Jeffry", "Vermont", "Williams"};

    static final int INTENT_REQUEST_TASK_ACTIVITY = 1;


    //private DBAdapter dba;



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == INTENT_REQUEST_TASK_ACTIVITY)
        {
            if(resultCode == RESULT_OK) {
                int index = data.getIntExtra("index", -1);

                Task t;
                String action = data.getStringExtra("action");
                switch (action){
                    case "save":
                        // String title = data.getStringExtra("title");

                        // t = Tasks.self.get(index);
                        // t.update(title);
                    break;

                    case "delete":
                        // t = Tasks.self.get(index);
                        // t.delete();
                    break;

                    case "new":
                    break;
                }


                sort();

            }

        }
    }


    private Toast toast;

    private void initUIToast() {
        // this.toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        this.toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
        // this.toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void toast (String text) {
        this.toast.setText(text);
        this.toast.show();
    }


    Snackbar snack;

    private void initUISnack(){
        View parentLayout = findViewById(android.R.id.content);
        this.snack = Snackbar.make(parentLayout, "", Snackbar.LENGTH_SHORT)
            .setAction("CLOSE", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            })
            .setActionTextColor(Color.parseColor("#aabbcc"));
    }

    private void snack (String text){
        snack.setText(text).show();
    }


    ActionBar abar;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void initUIActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // ActionBar ab = getActionBar();
            abar = getSupportActionBar();
            abar.setTitle(R.string.app_name);
            abar.setSubtitle(R.string.app_desc);

            abar.setDisplayShowHomeEnabled(true);
            // abar.setIcon(R.drawable.ic_launcher_tl);
            abar.setDisplayUseLogoEnabled(true);
            abar.setIcon(R.mipmap.ic_logo);

        }
    }


    private int sortTypeId = 0;

    static final int SORT_BY_TITLE_ASC = 0;
    static final int SORT_BY_TITLE_DESC = 1;
    static final int SORT_BY_TIMESTAMP_ASC = 2;
    static final int SORT_BY_TIMESTAMP_DESC = 3;

    private void sort (){
        sort(sortTypeId);
    }

    private void sort (int sortTypeId){
        ListView lv = findViewById(R.id.listView);

        TaskListViewAdapter ad = (TaskListViewAdapter) lv.getAdapter();

        if (ad != null) { // TODO: refactor, do not call when not ready
            switch (sortTypeId) {
                case SORT_BY_TITLE_ASC:
                    ad.sortByTitle(TaskListViewAdapter.SORT_ASC);
                    break;
                case SORT_BY_TITLE_DESC:
                    ad.sortByTitle(TaskListViewAdapter.SORT_DESC);
                    break;
                case SORT_BY_TIMESTAMP_ASC:
                    ad.sortByTimestamp(TaskListViewAdapter.SORT_ASC);
                    break;
                case SORT_BY_TIMESTAMP_DESC:
                    ad.sortByTimestamp(TaskListViewAdapter.SORT_DESC);
                    break;
            }

            this.sortTypeId = sortTypeId;
        }
    }

    private void initUI(){
        initUIToast();
        initUISnack();
        initUIActionBar();

        Button btnSort = findViewById(R.id.btnSort);
        btnSort.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final CharSequence[] opts = {"by title A->Z", "by title Z->A", "newest first", "oldest first"};


                getAlertDialogBuilder("Select sorting:", null)
                .setSingleChoiceItems(opts, sortTypeId,
                    new DialogInterface.OnClickListener() {
                        int selectedIndex;

                        public void onClick(DialogInterface dialog, int index) {
                            sort(index);

                            dialog.dismiss();

                            snack(opts[index].toString());
                        }
                    })
                .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        // buttonView.setChecked(false);
                    }
                })
                .show();
            }
        });

        Button btnReset = findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAlertDialogBuilder("Reset all", "Really reset all tasks?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Tasks.getInstance().resetAll();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // NO
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
            }
        });

    }


    public void updateListViewItemTime(int index, long elapsed, long elapsedTotal, boolean odd){
        ListView lv = findViewById(R.id.listView);

        View iv = Utils.findListViewItemByIndex(index, lv);

        if (iv == null) return;


        TaskListViewAdapter.updateItemTime((ViewGroup) iv, elapsed, elapsedTotal, odd);
    }


    void listViewChangeHandler(){
        ListView lv = findViewById(R.id.listView);

        ((TaskListViewAdapter)(lv.getAdapter())).notifyDataSetChanged();

        updateTaskTotals();
    }


    private Tasks.Listener tasksListener = null;

    public void initListView2 (){
        final ListView lv = findViewById(R.id.listView);

        // TASK VIEW ADAPTER

        tasksListener = new Tasks.Listener () {
            public void onTicker(Tasks tasks, ArrayList<Task> tasksActive, boolean odd){
                runOnUiThread(new Tasks.ListenerTickerRunnable() {
                    @Override
                    public void go() { // loop thru all active tasks and update their list items
                    //log("odd: " + odd);
                    for (Task at : tasksActive) {
                        int index = tasks.indexOf(at);

                        updateListViewItemTime(index, at.getElapsed(), at.getElapsedTotal(), odd);
                    }

                    /*
                    if (TLApplication.taskActivity != null) {
                        TaskActivity a = TLApplication.taskActivity;

                        Task t = a.getTask();

                        if (t.isRunning())
                        {
                            TextView ed = a.findViewById(R.id.elapsed);
                            TextView et = a.findViewById(R.id.elapsedTotal);

                            TimeFrac tfDelta = new TimeFrac(t.getElapsed(), odd);
                            TimeFrac tfTotal = new TimeFrac(t.getElapsedTotal(), odd);

                            ed.setText(tfDelta.toString("%h%:%m%:%s%"));
                            et.setText(tfTotal.toString("%h%:%m%:%s%"));
                        }
                    }
                    */

                    updateTaskTotals();
                    }
                }.init(tasks, tasksActive, odd));
            }

            public void onStart(int index, Task t)
            {
                runOnUiThread(new Tasks.ListenerRunnable (){
                    @Override
                    public void go() {
                        View iv = Utils.findListViewItemByIndex(index, lv);

                        if (iv == null) return;

                        TaskListViewAdapter.setItemStyle((ViewGroup) iv, TaskListViewAdapter.STATE.RUNNING);


                        snack(String.format("started \"%s\"", task.title));
                    }
                }.init(index, t));
            }

            public void onStop(int index, Task t)
            {
                runOnUiThread(new Tasks.ListenerRunnable (){
                    @Override
                    public void go() {
                        View iv = Utils.findListViewItemByIndex(index, lv);

                        if (iv == null) return;

                        updateListViewItemTime(index, 0, task.getElapsedTotal(), true);
                        TaskListViewAdapter.setItemStyle((ViewGroup) iv, TaskListViewAdapter.STATE.DEFAULT);


                        updateTaskTotals();


                        snack(String.format("stopped \"%s\"", task.title));
                    }
                }.init(index, t));
            }

            public void onReset(int index, Task t)
            {
                runOnUiThread(new Tasks.ListenerRunnable (){
                    @Override
                    public void go() {
                        View iv = Utils.findListViewItemByIndex(index, lv);

                        if (iv == null) return;

                        updateListViewItemTime(index, 0, task.getElapsedTotal(), true);
                        TaskListViewAdapter.setItemStyle((ViewGroup) iv, TaskListViewAdapter.STATE.VIRGIN);


                        updateTaskTotals();


                        snack(String.format("reset \"%s\"", task.title));
                    }
                }.init(index, t));
            }

            public void onUpdate(int index, Task t) {
                runOnUiThread(new Tasks.ListenerRunnable (){
                    @Override
                    public void go(){
                        // View iv = lv.getChildAt(index);
                        View iv = Utils.findListViewItemByIndex(index, lv);

                        TextView tv = (TextView) ((ViewGroup) iv).getChildAt(1);
                        tv.setText(task.title);
                    }
                }.init(index, t));
            }

            public void onDelete(int index, Task t) {
                runOnUiThread(new Runnable(){
                    @Override
                    public void run(){
                        listViewChangeHandler();
                    }
                });

            }

            public void onCreate(int index, Task t) {
                runOnUiThread(new Runnable(){
                    @Override
                    public void run(){
                        listViewChangeHandler();
                    }
                });

            }
        };


        service.getTasks().addListener(tasksListener);

        TaskListViewAdapter aa = new TaskListViewAdapter(this, service.getTasks());
        lv.setAdapter(aa);

        registerForContextMenu(lv);

        // Emulate "new task" click

        /*
        lv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // openTask(-1);
            }
        });
        */

        /*
        lv.setOnItemClickListener( new AdapterView.OnItemClickListener()
        {
            private ListView lv;

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = (String) lv.getItemAtPosition(position);

                log(value);
            }

            private AdapterView.OnItemClickListener init(ListView lv){
                this.lv = lv;

                return this;
            }
        }.init(lv));
        */

        log("initial SORT");

        sort(sortTypeId); // initial sort

        updateTaskTotals();
    }

    public void updateTaskTotals(){
        Tasks.Totals totals = Tasks.getInstance().getTotals();

        TimeFrac tfTotal = new TimeFrac(totals.elapsed, false);

        TextView tt = findViewById(R.id.total);
        tt.setText(Utils.format("Total: %d task(s), %s", totals.count, tfTotal));
    }

    public void openTask(int index){
        Intent intent = new Intent(this, TaskActivity.class);
        //EditText editText = (EditText) findViewById(R.id.editText);
        //String message = editText.getText().toString();
        intent.putExtra("index", index);

        startActivityForResult(intent, INTENT_REQUEST_TASK_ACTIVITY);
    }


    void initAll(){
        initUI();

        //initDBA();
        //initTasks();

        initListView2();


        SwitchCompat ss = findViewById(R.id.singleSwitch);
        ss.setChecked(service.getTasks().isSingle());

        ss.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, boolean trySingle) {
                Tasks ts = service.getTasks();
                boolean wasSingle = ts.isSingle();

                if (! wasSingle && trySingle){

                    if (ts.getActiveTaskCount() > 1) {

                        final ArrayList<Task.Info> atis = ts.enumActiveTasksInfo();

                        final ArrayList<String> titles = new ArrayList<>();
                        for (Task.Info ti : atis) {
                            titles.add(ti.title);
                        }

                        getAlertDialogBuilder("Select task to keep:", null)
                        .setItems(titles.toArray(new CharSequence[titles.size()]),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Tasks ts = service.getTasks();

                                    // The 'which' argument contains the index position of the selected item
                                    Task.Info ti = atis.get(which);
                                    Task t = ts.findById(ti.id);

                                    if (t == null) throw new RuntimeException("task not found having ID:" + ti.id);

                                    // TODO: implement stopall call inside Tasks class on single mode change (possibly accept picked task for single mode through params ?)
                                    ts.stopAll(t);
                                    // t.start();
                                    ts.setSingle(true);
                                }
                            })
                        .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                buttonView.setChecked(false);
                            }
                        })
                        .show();

                        // getAlertDialogBuilder("Single mode", "You are about to switch to single mode while more than 1 task is running. ")
                    } else {
                        ts.setSingle(true);
                    }

                }
                else if (wasSingle && ! trySingle){
                    ts.setSingle(false);
                }
            }
        });


        ConstraintLayout cl = findViewById(R.id.mainLayout);
        cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        testNotification();
                        toast("notified");
                    }
                }, 1000);

                */

                // testService();

            }
        });
    }


    private TLService service;
    boolean serviceBound = false;
    private ServiceConnection serviceConnection;

    void initService(){
        log("initService()");
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder ibinder) {
                log("ServiceConnected");

                TLService.TLServiceBinder binder = (TLService.TLServiceBinder) ibinder;

                service = (TLService) binder.getService();
                serviceBound = true;


                initAll();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                log("ServiceDisconnected");

                service = null;
                serviceBound = false;
            }
        };

        log("bindService()");
        Intent intent = new Intent(this, TLService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    void cleanupService(){
        log("cleanupService()");

        if (serviceBound) {
            log("unbindService()");
            unbindService(serviceConnection);
            serviceBound = false;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        log("onCreate");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        initService();
    }

    @Override
    protected void onDestroy() {
        log("onDestroy");


        if (tasksListener != null) {
            service.getTasks().removeListener(tasksListener);
            tasksListener = null;
        }

        cleanupService();


        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        //toast("ON RESTART");
        log("onRESTART");

        super.onRestart();

    }

    @Override
    protected void onStart() {
        //toast("ON START");
        log("onSTART");

        super.onStart();
    }

    @Override
    protected void onResume() {
        //toast("ON RESUME");
        log("onRESUME");

        super.onResume();

    }

    @Override
    protected void onPause() {
        //toast("ON PAUSE");
        log("onPAUSE");

        super.onPause();
    }

    @Override
    protected void onStop() {
        //toast("ON STOP");
        log("onSTOP");

        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
                openTask(-1);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.listView) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            Task t = (Task) lv.getItemAtPosition(acmi.position);


            menu.setHeaderTitle(String.format("Task \"%s\":", t.title));

            menu.add("modify...");
            menu.add("reset");
            menu.add("delete");
        }
    }


    public void doActionDelete(int index, Task task){
        Actions.delete(this, index, task, new Actions.Listener(){
            @Override
            public void onDone(Map<String, String> data) {
                // listViewChangeHandler();
            }
        });
    }

    public void doActionReset(int index, Task task){
        Actions.reset(this, index, task, new Actions.Listener(){
            @Override
            public void onDone(Map<String, String> data) {
                // listViewChangeHandler();
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        boolean found = false;

        AdapterView.AdapterContextMenuInfo minfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        ListView lv = findViewById(R.id.listView);
        int index = minfo.position;

        Task t = (Task) lv.getItemAtPosition(index);

        if (item.getTitle()      == "modify...") {
            openTask(index);
            found = true;
        }
        else if (item.getTitle() == "reset") {
            doActionReset(index, t);
            found = true;
        }
        else if (item.getTitle() == "delete") {
            doActionDelete(index, t);
            found = true;
        }
        if (found) return true;

       return super.onContextItemSelected(item);
    }


    /**
     *  OBSOLETE
     */

    /*

    public void initListView (){
        ListView lv = findViewById(R.id.listView);
        ArrayAdapter<String> aa = new ArrayAdapter<String> (this, R.layout.list_item, listItems);
        lv.setAdapter(aa);
        lv.setOnItemClickListener( new AdapterView.OnItemClickListener()
        {
            private ListView lv;
            private Toast toast;

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = (String) lv.getItemAtPosition(position);

                this.toast.setText("pos: " + position + "; val: " + value);
                this.toast.show();

            }

            private AdapterView.OnItemClickListener init(ListView lv, Toast toast){
                this.lv = lv;
                this.toast = toast;

                return this;
            }
        }.init(lv, this.toast));

        // aa.addAll("aaa", "bbb", "ccc");
        // aa.add("xxx");
    }

    public void initWebView() {
        WebView wv = (WebView) this.findViewById(R.id.webView);


        WebSettings ws = wv.getSettings();
        ws.setJavaScriptEnabled(true);
        // ws.setBuiltInZoomControls(true);
        // ws.setSupportZoom(true);
        ws.setLoadWithOverviewMode(true);
        ws.setUseWideViewPort(true);

        // String summary = "<html><body>You scored <b>192</b> points.</body></html>";
        // wv.loadData(summary, "text/html", null);


        wv.loadUrl("http://roman.tsourick.com/");
        wv.setInitialScale(1);
    }

    private void initCountdown(){
        final TextView countDown = this.findViewById(R.id.countDown);

        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                countDown.setText(millisUntilFinished / 1000 + " seconds...");
            }

            public void onFinish() {
                countDown.setText("done!");
            }
        }.start();
    }

    private void testScheduler(){
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        // ScheduledFuture<?> future = null;

        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                toast("every 3 sec!");
            }
        }, 0, 3, TimeUnit.SECONDS);
    }
    */


    public void testNotification()
    {
        int runningTaskNumber = 3;

        Class<MainActivity> ResultActivityClass = MainActivity.class;

        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
            // .setSmallIcon(R.mipmap.ic_launcher_tl_round)
            .setSmallIcon(R.mipmap.ic_launcher_tl_round)
            .setContentTitle("Timelancer")
            .setContentText(String.format("Running %s tasks", runningTaskNumber))
            .setNumber(runningTaskNumber)
            .setTicker(String.format("Running %s tasks", runningTaskNumber));


        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        String[] events = {"task one", "task two", "task four", "task five", "task N"};// new String[6];
        // Sets a title for the Inbox in expanded layout
        inboxStyle.setBigContentTitle(String.format("Running %s tasks", runningTaskNumber) + ":");

        // Moves events into the expanded layout
        for (String e : events) {
            inboxStyle.addLine(e);
        }


        // Moves the expanded layout object into the notification object.
        mBuilder.setStyle(inboxStyle);


        Intent resultIntent = new Intent(this, ResultActivityClass);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(ResultActivityClass);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        mBuilder.setContentIntent(resultPendingIntent);

        Notification nn = mBuilder.build();


        int mNotificationId = 1;

        NotificationManager mgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mgr.notify(mNotificationId, nn);


    }

    public AlertDialog.Builder getAlertDialogBuilder(String title, String message){
        return Utils.getAlertDialogBuilder(this, title, message);
    }

    public void testService(){
        class Starter {
            public Starter(String action){
                Intent intent = new Intent(TLApplication.getAppContext(), TLService.class);
                intent.putExtra("action", action);
                startService(intent);

                // Log.v("TL", "after startService()");

                // int sv = TLService.getInstance().getServiceVar();

                // toast(sv+"");
            }
        }


        boolean running = Utils.isServiceRunning(TLService.class, this);

        String[] items = (running ? new String[] {"GO", "NOTIFY", "START FG", "STOP FG", "STOP", "CANCEL"} : new String[] {"GO", "NOTIFY", "START FG", "STOP FG", "CANCEL"});

        getAlertDialogBuilder("Select action:", null)
                /*
            .setTitle("Question")
            .setMessage("Select action")
            .setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            })
            .setPositiveButton("GO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    new Starter("go");
                }
            })
            .setNegativeButton("STOP", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    new Starter("stop");
                }
            })
            */

            .setItems(items,
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // The 'which' argument contains the index position
                    // of the selected item
                    switch (which) {
                        case 0:
                            new Starter("start");
                            break;
                        case 1:
                            new Starter("notify");
                            break;
                        case 2:
                            new Starter("startfg");
                            break;
                        case 3:
                            new Starter("stopfg");
                            break;
                        case 4:
                            new Starter("stop");
                            break;
                        case 5:
                            break;
                    }
                }
            })
           // .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
    }

}