package com.helloworldstudio.timelancer;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.View;
import android.widget.ListView;

/**
 * Created by pe60 on 17.07.2017.
 */

public class Utils {
    /**
     * @param index global index wihtin the list
     */
    public static View findListViewItemByIndex(int index, ListView listView) {

        int firstListItemPosition = listView.getFirstVisiblePosition();
        int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (index < firstListItemPosition || index > lastListItemPosition) {
            // System.out.println("Invisible view!");
            return null;
            //return listView.getAdapter().getView(index, null, listView);

            //return null;
        } else {
            int childIndex = index - firstListItemPosition + listView.getHeaderViewsCount();
            return listView.getChildAt(childIndex);
        }
    }

    public static boolean isServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static AlertDialog.Builder getAlertDialogBuilder(Context x, String title, String message) {
        AlertDialog.Builder builder;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(x, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(x);
        }

        if (title != null) builder.setTitle(title);
        if (message != null) builder.setMessage(message);

        return builder;
    }


    public static void alertException(Context x, String msg, String type) {
        getAlertDialogBuilder(x, "Exception" + ((type != "") ? " \"" + type + "\"" : ""), msg)
                //.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {// YES
                    }
                })
            /*
            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) { // NO
                }
            })
            */
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static void alertException(Context x, String msg) {
        alertException(x, msg, "");
    }

    public static String format(String format, Object... args){
        return String.format(java.util.Locale.US, format, args);
    }

}
