package com.helloworldstudio.timelancer;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by pe60 on 28.06.2017.
 */

public class DBAdapter {

    private final Context context;
    private DBHelper dbh;
    private SQLiteDatabase db;
    // private DBContract dbc;

    public DBAdapter (Context c) {
        this.context = c;
        this.dbh = new DBHelper(c);
    }


    public DBAdapter open() throws SQLException
    {
        db = dbh.getWritableDatabase();
        return this;
    }

    public void close()
    {
        dbh.close();
    }


    public long insertTask(String title, int state)
    {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.TaskTable.COLUMN_NAME_TITLE, title);
        cv.put(DBContract.TaskTable.COLUMN_NAME_STATE, state);

        return db.insert(DBContract.TaskTable.TABLE_NAME, null, cv);
    }

    public int deleteTask(long rowId)
    {
        return db.delete(DBContract.TaskTable.TABLE_NAME, DBContract.TaskTable._ID + "=" + rowId, null);
    }

    public ArrayList<DBContract.TaskRow> getAllTasks()
    {
        final Cursor x = db.query(DBContract.TaskTable.TABLE_NAME, new String[] {
                        DBContract.TaskTable._ID,
                        DBContract.TaskTable.COLUMN_NAME_TITLE,
                        DBContract.TaskTable.COLUMN_NAME_TS,
                        DBContract.TaskTable.COLUMN_NAME_STATE,
                        DBContract.TaskTable.COLUMN_NAME_ELAPSED_TOTAL
        }, null, null, null, null, null);

        ArrayList<DBContract.TaskRow> trs = new ArrayList<DBContract.TaskRow>();
        if (x.moveToFirst()){
            do{
                DBContract.TaskRow tr = new DBContract.TaskRow(){{
                    id           = x.getInt   (x.getColumnIndex(DBContract.TaskTable._ID));
                    title        = x.getString(x.getColumnIndex(DBContract.TaskTable.COLUMN_NAME_TITLE));
                    ts           = x.getString(x.getColumnIndex(DBContract.TaskTable.COLUMN_NAME_TS));
                    state        = x.getInt   (x.getColumnIndex(DBContract.TaskTable.COLUMN_NAME_STATE));
                    elapsedTotal = x.getLong  (x.getColumnIndex(DBContract.TaskTable.COLUMN_NAME_ELAPSED_TOTAL));
                }};

                trs.add(tr);
            }while (x.moveToNext());
            x.close();
        }

        return trs;
    }

    public Cursor getTask(long rowId) throws SQLException
    {
        Cursor s = db.query(true, DBContract.TaskTable.TABLE_NAME, new String[] {
                DBContract.TaskTable._ID,
                DBContract.TaskTable.COLUMN_NAME_TITLE,
                DBContract.TaskTable.COLUMN_NAME_TS,
                DBContract.TaskTable.COLUMN_NAME_STATE,
                DBContract.TaskTable.COLUMN_NAME_ELAPSED_TOTAL
            },
            DBContract.TaskTable._ID + "=" + rowId,
            null, null, null, null, null
        );
        if (s != null) {
            s.moveToFirst();
        }
        return s;
    }
    public DBContract.TaskRow getTaskRow(Cursor s) throws SQLException {
        final Cursor x = s;
        DBContract.TaskRow tr = new DBContract.TaskRow(){{
                id           = x.getInt   (x.getColumnIndex(DBContract.TaskTable._ID));
                title        = x.getString(x.getColumnIndex(DBContract.TaskTable.COLUMN_NAME_TITLE));
                ts           = x.getString(x.getColumnIndex(DBContract.TaskTable.COLUMN_NAME_TS));
                state        = x.getInt   (x.getColumnIndex(DBContract.TaskTable.COLUMN_NAME_STATE));
                elapsedTotal = x.getLong  (x.getColumnIndex(DBContract.TaskTable.COLUMN_NAME_ELAPSED_TOTAL));
            }};

        return tr;
    }

    private boolean updateTask(long rowId, ContentValues cv){
        return db.update(DBContract.TaskTable.TABLE_NAME, cv, DBContract.TaskTable._ID + "=" + rowId, null) > 0;
    }

    public boolean updateTask(long rowId, String title, int state, long elapsedTotal)
    {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.TaskTable.COLUMN_NAME_TITLE, title);
        cv.put(DBContract.TaskTable.COLUMN_NAME_STATE, state);
        cv.put(DBContract.TaskTable.COLUMN_NAME_ELAPSED_TOTAL, elapsedTotal);

        return updateTask(rowId, cv);
    }

    public boolean updateTaskTitle(long rowId, String title)
    {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.TaskTable.COLUMN_NAME_TITLE, title);

        return updateTask(rowId, cv);
    }

    /*
    public boolean updateTaskElapsedTotal(long rowId, long elapsedTotal)
    {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.TaskTable.COLUMN_NAME_ELAPSED_TOTAL, elapsedTotal);

        return updateTask(rowId, cv);
    }
    */

    public boolean updateTaskState(long rowId, int state, long elapsedTotal)
    {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.TaskTable.COLUMN_NAME_STATE, state);
        cv.put(DBContract.TaskTable.COLUMN_NAME_ELAPSED_TOTAL, elapsedTotal);

        return updateTask(rowId, cv);
    }
    public boolean updateTaskState(long rowId, int state)
    {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.TaskTable.COLUMN_NAME_STATE, state);

        return updateTask(rowId, cv);
    }

    public boolean resetTask(long rowId)
    {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.TaskTable.COLUMN_NAME_TS, 0);
        cv.put(DBContract.TaskTable.COLUMN_NAME_STATE, 0);
        cv.put(DBContract.TaskTable.COLUMN_NAME_ELAPSED_TOTAL, 0);

        return updateTask(rowId, cv);
    }
}
