package com.helloworldstudio.timelancer;

import android.provider.BaseColumns;

/**
 * Created by pe60 on 28.06.2017.
 */

public final class DBContract {
    // If you change the database schema, you must increment the database version.
    public static final  int    DATABASE_VERSION   = 2;
    public static final  String DATABASE_NAME      = "database.db";
    // private static final String TEXT_TYPE          = " TEXT";
    // private static final String COMMA_SEP          = ",";

    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private DBContract() {}

    /* Inner class that defines the table contents */
    public static class TaskTable implements BaseColumns {
        public static final String TABLE_NAME        = "task";

        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_TS    = "ts";
        public static final String COLUMN_NAME_STATE = "state";
        public static final String COLUMN_NAME_ELAPSED_TOTAL = "elapsed_total";

        public static final int COLUMN_VALUE_STATE_DEFAULT = 0;
        public static final int COLUMN_VALUE_STATE_RUNNING = 1;

        public static final String SQL_CREATE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID                          + " INTEGER PRIMARY KEY," +
                COLUMN_NAME_TITLE            + " TEXT," +
                COLUMN_NAME_TS               + " DATETIME DEFAULT CURRENT_TIMESTAMP," +
                COLUMN_NAME_STATE            + " INTEGER DEFAULT 0," +
                COLUMN_NAME_ELAPSED_TOTAL    + " INTEGER DEFAULT 0)";

        public static final String SQL_INSERT_INIT =
            String.format("INSERT INTO %s (%s) VALUES (\"%s\"),(\"%s\"),(\"%s\")",
                TABLE_NAME,
                COLUMN_NAME_TITLE,
                "task A", "task B", "task C"
            );

        public static final String SQL_DELETE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static class TaskRow {
        public int     id;
        public String  title;
        public String  ts;
        public int     state;
        public long    elapsedTotal;
    }
}
